import logo from './logo.svg';
import './App.css';
import React from 'react';
import {
  BrowserRouter,
  Route,
  Link,
  Switch as BasicSwitch,
  useLocation
} from "react-router-dom";

import Home from './page/home'
import Myport from './page/myport'
import Geo from './page/geo'
import { connect } from 'react-redux';


var checkloop;
function App({pageSection}) {
  let location = useLocation()
React.useEffect(() => {
  document.title = pageSection
}, [pageSection])
  return (
    <div>
     <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top">
  <div className="container-fluid">
    <Link className="navbar-brand" to="/">Test Redux</Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <Link className={"nav-link" + (location.pathname == '/' ? ' active' : '')} to="/">Get API</Link>
        </li>
        <li className="nav-item">
          <Link className={"nav-link" + (location.pathname == '/myport' ? ' active' : '')} to="/myport">MyPort</Link>
        </li>
        <li className="nav-item">
          <Link className={"nav-link" + (location.pathname == '/geo' ? ' active' : '')} to="/geo">Geo API</Link>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div className='mt-5 container'>
<BasicSwitch>
  <Route exact path="/" render={() => <Home />} />
  <Route path="/myport" render={() => <Myport />} />
  <Route path="/geo" render={() => <Geo />} />
</BasicSwitch>
</div>
<footer onClick={() => window.open('//status.cpxdev.tk', '_blank').focus()} className='text-center pt-3 pb-3'>
   Copyright {new Date().getFullYear()} CPXDevStudio, Allright Reserved
</footer>
    </div>
  );
}


const mapStateToProps = (state) => ({
  pageSection: state.pageSection,
});

export default connect(mapStateToProps)(App);
