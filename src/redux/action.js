export function changePageName(val) {
    return {
      type: 'PAGE_CHANGE',
      val: val
    };
  }