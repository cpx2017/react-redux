const initialState = {
    pageSection: 'Loading',
  };
  
  function Reducer(state = initialState, action) {
    switch(action.type) {
      case 'PAGE_CHANGE':
        return { ...state, pageSection: action.val };
      default:
        return state;
    }
  }
  
  export default Reducer;
  