import React from 'react';
import { connect } from 'react-redux';

import {
  changePageName
} from '../redux/action';

const MyPort = ({changePageNameAction}) => {
    React.useEffect(() => {
        changePageNameAction('Myport')
    }, [])
    return ( 
      <>MYP</>
     );
}
 
const mapDispatchToProps = (dispatch) => ({
    changePageNameAction: (val) => dispatch(changePageName(val))
  });
export default connect(null, mapDispatchToProps)(MyPort);
