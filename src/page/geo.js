import React from 'react';
import { connect } from 'react-redux';

import {
  changePageName
} from '../redux/action';

const Geo = ({changePageNameAction}) => {
    React.useEffect(() => {
        changePageNameAction('Geolocation')
    }, [])
    return ( 
        <>Geo</>
     );
}

  
  const mapDispatchToProps = (dispatch) => ({
    changePageNameAction: (val) => dispatch(changePageName(val))
  });
export default connect(null, mapDispatchToProps)(Geo);
