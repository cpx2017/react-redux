import React from 'react'
import { connect } from 'react-redux';

import {
  changePageName
} from '../redux/action';

const Home = ({changePageNameAction}) => {
    React.useEffect(() => {
        changePageNameAction('Home')
    }, [])
    return <>Home</>
}

const mapDispatchToProps = (dispatch) => ({
    changePageNameAction: (val) => dispatch(changePageName(val))
  });
export default connect(null, mapDispatchToProps)(Home);